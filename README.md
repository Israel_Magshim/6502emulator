# 6502Emu

## Overview

6502Emu is a C++ emulator for the 6502 microprocessor. This project aims to provide an accurate simulation of the 6502 CPU, including its instruction set and hardware interactions. It is designed to help understand the low-level workings of microprocessors and deepen knowledge of low-level programming.

## Features

- Full emulation of the 6502 CPU instruction set.
- Supports various addressing modes.
- Implements cycle-accurate timing.
- Debugging features for tracing and inspecting CPU state.
- Written in C++ for performance and portability.

## Requirements

- C++20 or higher
- A C++ compiler (GCC, Clang, or MSVC)
- Git (for cloning the repository)

## Installation

1. Clone the repository:
    ```sh
    git clone https://gitlab.com/Israel_Magshim/6502emulator/
    cd 6502Emu
    ```

2. Build the project:
    ```sh
    Open in Visual Studio 
    ```

## Usage

To run the emulator, use the following command:
```sh
./6502Emu [options]
```

## Contributing

Contributions are welcome! Please feel free to submit a Pull Request or open an issue.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.
---
