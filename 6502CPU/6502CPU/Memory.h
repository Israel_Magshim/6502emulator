#pragma once
#include "Definitions.h"
#include <stdexcept>

#define MEM_SIZE 0xFFFF
#define ZERO_PAGE_SIZE 0xFF

class Memory
{
public:
	Byte& operator[](Word address);
    

private:
	Byte _mem[MEM_SIZE]{0};
};

