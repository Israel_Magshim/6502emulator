#pragma once

using Byte = unsigned char;
using Word = unsigned short;

//If you want the k-th bit of n, then do
#define BIT_VALUE(N, K) ((N & ( 1 << K )) >> K)

#define NEGTIVE_BIT_A 7
#define WORD_ADRRESS_FROM_BYTES(LowByte, HighByte) ( (LowByte) | ( (HighByte) << 8) )
#define OVERFLOW_ONE_BYTE_ADDITION(A, B) ( (A + B) > 0xff )
#define GET_OVERFLOW_ONE_BYTE_ADDITION(A, B) ( OVERFLOW_ONE_BYTE_ADDITION(A, B) ? (A + B) >> 8 : 0 )
#define SHIFT_RIGHT(A, NUM_OF_BITS) (A >> NUM_OF_BITS)

//conditional Expression ? expression1 : expression2;

