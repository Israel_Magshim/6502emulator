#include "Memory.h"


Byte& Memory::operator[](Word address)
{
	if (address >= 0 && address < MEM_SIZE)
	{
		return this->_mem[address];
	}
	throw std::range_error("Address Invalid");
}


/* TODO
Byte& Memory::operator=(Byte value)
{
	if (value > 0xFF) {
		throw std::range_error("Value too large for a byte");
	}
	byte_ref = value;
}

*/
