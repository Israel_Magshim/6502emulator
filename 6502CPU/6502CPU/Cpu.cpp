#include "Cpu.h"

Cpu::Cpu()
{
	//registers.
	this->A = 0;
	this->X = 0;
	this->Y = 0;
	this->SP = 0; // $FFFC and $FFFD (called the reset vector) 
	this->IP = 0xFFFC; // 0xFFFC - initial value
}

Word Cpu::getIP() const
{
	return this->IP;
}

void Cpu::incIP()
{
	this->IP++;
}

void Cpu::setA(Byte data)
{
	this->A = data;
}

Byte Cpu::getA() const
{
	return this->A;
}

Byte Cpu::getX() const
{
	return this->X;
}

void Cpu::setX(Byte data)
{
	this->X = data;
}

Byte Cpu::getY() const
{
	return this->Y;
}

void Cpu::setY(Byte data)
{
	this->Y = data;
}

Flags Cpu::getFlags() const
{
	return P;
}

void Cpu::updateFlagZero(const Byte& Register)
{
	if (fZero(Register))
	{
		this->P.Z = true;
	}
	else
	{
		this->P.Z = false;
	}
}

void Cpu::updateFlagNegtive(const Byte& Register)
{
	if (fNegtive(Register))
	{
		this->P.N = true;
	}
	else
	{
		this->P.N = false;
	}
}

std::ostream& operator<<(std::ostream& os, const Flags& dt)
{
	os << 
		"bit ->   7                           0" << std::endl <<
		"       +---+---+---+---+---+---+---+---+" << std::endl <<
		"       | N | V |   | B | D | I | Z | C |  <-- flag, 0/1 = reset/set" << std::endl <<
		"       +---+---+---+---+---+---+---+---+" << std::endl <<
		std::format("         {}   {}       {}   {}   {}   {}   {}\n", dt.N, dt.V, dt.B, dt.D, dt.I, dt.Z, dt.C);
	return os;
}

std::ostream& operator<<(std::ostream& os, const Cpu& dt)
{
	os << std::format("A:  {} \n", dt.A) <<
		std::format("X:  {} \n", dt.X) <<
		std::format("Y:  {} \n", dt.Y) <<
		std::format("SP: {} \n", dt.SP) <<
		std::format("IP: {} \n", dt.IP) <<
		dt.P;
	return os;
}

Flags::Flags()
{
	//flags.
	this->N = 0; // 1
	this->V = 0; // 2
	this->B = 0; // 3 
	this->D = 0; // 4
	this->I = 0; // 5
	this->Z = 0; // 6
	this->C = 0; // 7
}

bool Cpu::fNegtive(Byte Register) const
{
	return (bool)BIT_VALUE(Register, NEGTIVE_BIT_A) == true;
}

bool Cpu::fZero(Byte Register) const
{
	return Register == 0;
}

void Cpu::updateFlags()
{
	
	if (true)
	{

	}
}
