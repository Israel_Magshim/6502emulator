#include "Computer.h"


void Computer::excute()
{
	// starting execute

	Byte instruction = _fetchByte(); // cycle 1

	switch (instruction)
	{
	case OpCodes::LDA_IMMEDIATE:
	{
		Byte data = addressingModeLoadImmediate();
		updateFlagsInstructionLoad(_cpu.A, data);
		break;
	}
	case OpCodes::LDA_ZERO_PAGE: // 3 cycles
	{
		Byte data = addressingModeLoadZeroPage();
		updateFlagsInstructionLoad(_cpu.A, data);

		break;
	}
	case OpCodes::LDA_ZERO_PAGE_X:
	{
		Byte data = addressingModeLoadZeroPageX();
		updateFlagsInstructionLoad(_cpu.A, data);

		break;
	}
	case OpCodes::LDA_ABSOLUTE:
	{
		Byte data = addressingModeLoadAbsolute();
		updateFlagsInstructionLoad(_cpu.A, data);

		break;
	}
	case OpCodes::LDA_ABSOLUTE_X:
	{
		Byte data = addressingModeAbsoluteX();

		updateFlagsInstructionLoad(_cpu.A, data);

		break;
	}
	case OpCodes::LDA_ABSOLUTE_Y:
	{
		Byte data = addressingModeAbsoluteY();
		updateFlagsInstructionLoad(_cpu.A, data);

		break;
	}
	case OpCodes::LDA_INDEXED_INDIRECT:
	{
		Byte data = addressingModeIndexedIndirect();
		updateFlagsInstructionLoad(_cpu.A, data);

		break;
	}
	case OpCodes::LDA_INDIRECT_INDEXED:
	{	
		Byte data = addressingModeIndirectIndexed();
		updateFlagsInstructionLoad(_cpu.A, data);

		break;
	}
	
	case OpCodes::LDX_IMMEDIATE:
	{
		Byte data = addressingModeLoadImmediate();
		updateFlagsInstructionLoad(_cpu.X, data);
		break;
	}
	case OpCodes::LDX_ZERO_PAGE:
	{
		Byte data = addressingModeLoadZeroPage();
		updateFlagsInstructionLoad(_cpu.X, data);
		break;
	}
	case OpCodes::LDX_ZERO_PAGE_Y:
	{
		Byte data = addressingModeLoadZeroPageY();
		updateFlagsInstructionLoad(_cpu.X, data);
		break;
	}
	case OpCodes::LDX_ABSOLUTE:
	{
		Byte data = addressingModeLoadAbsolute();
		updateFlagsInstructionLoad(_cpu.X, data);
		break;
	}
	case OpCodes::LDX_ABSOLUTE_Y:
	{
		Byte data = addressingModeAbsoluteY();
		updateFlagsInstructionLoad(_cpu.X, data);
		break;
	}


	case OpCodes::LDY_IMMEDIATE:
	{
		Byte data = addressingModeLoadImmediate();
		updateFlagsInstructionLoad(_cpu.Y, data);
		break;
	}
	case OpCodes::LDY_ZERO_PAGE:
	{
		Byte data = addressingModeLoadZeroPage();
		updateFlagsInstructionLoad(_cpu.Y, data);
		break;
	}
	case OpCodes::LDY_ZERO_PAGE_X:
	{
		Byte data = addressingModeLoadZeroPageX();
		updateFlagsInstructionLoad(_cpu.Y, data);
		break;
	}
	case OpCodes::LDY_ABSOLUTE:
	{
		Byte data = addressingModeLoadAbsolute();
		updateFlagsInstructionLoad(_cpu.Y, data);
		break;
	}
	case OpCodes::LDY_ABSOLUTE_X:
	{
		Byte data = addressingModeAbsoluteX();
		updateFlagsInstructionLoad(_cpu.Y, data);
		break;
	}

	case OpCodes::LSR_ACCUMULATOR:
	{
		_cpu.P.C = BIT_VALUE(_cpu.getA(), 0);
		_cpu.setA(SHIFT_RIGHT(_cpu.getA(), 1));
		_cycles++;
		_cpu.updateFlagNegtive(_cpu.getA());
		_cpu.updateFlagZero(_cpu.getA());

		break;
	}
	case OpCodes::LSR_ZERO_PAGE:
	{
		Byte& data = addressingModeLoadZeroPage();
		Byte dataCopy = data;
		data = SHIFT_RIGHT(data, 1);
		_cycles++;
		_cpu.P.C = BIT_VALUE(dataCopy, 0);
		_cycles++;
		_cpu.updateFlagNegtive(dataCopy);
		_cpu.updateFlagZero(dataCopy);

		break;
	}
	case OpCodes::LSR_ZERO_PAGE_X:
	{
		Byte& data = addressingModeLoadZeroPageX();
		Byte dataCopy = data;
		data = SHIFT_RIGHT(data, 1);
		_cycles++;
		_cpu.P.C = BIT_VALUE(dataCopy, 0);
		_cycles++;
		_cpu.updateFlagNegtive(dataCopy);
		_cpu.updateFlagZero(dataCopy);
		break;
	}
	case OpCodes::LSR_ABSOLUTE:
	{
		Byte& data = addressingModeLoadAbsolute();
		Byte dataCopy = data;
		data = SHIFT_RIGHT(data, 1);
		_cycles++;
		_cpu.P.C = BIT_VALUE(dataCopy, 0);
		_cycles++;
		_cpu.updateFlagNegtive(dataCopy);
		_cpu.updateFlagZero(dataCopy);
		break;
	}
	case OpCodes::LSR_ABSOLUTE_X:
	{
		Byte& data = addressingModeAbsoluteX();
		Byte dataCopy = data;
		data = SHIFT_RIGHT(data, 1);
		_cycles++;
		_cpu.P.C = BIT_VALUE(dataCopy, 0);
		_cycles++;
		_cpu.updateFlagNegtive(dataCopy);
		_cpu.updateFlagZero(dataCopy);
		break;
	}


	default:
	{
		throw std::exception("InValid OpCode");

	}

	}

}

int Computer::getCycles() const
{
	return _cycles;
}

Computer::Computer(const Cpu& cpu, const Memory& mem)
{
	_cpu = cpu;
	_mem = mem;
	_cycles = 0;
}

Flags Computer::getFlags() const
{
	return _cpu.getFlags();
}

Cpu Computer::getCpu() const
{
	return _cpu;
}

Memory Computer::getMemory() const
{
	return _mem;
}

Byte& Computer::_fetchByte()
{
	Word address = _cpu.getIP();
	Byte& data = _fetchByte(address);
	_cpu.incIP();
	
	return data;
}

Byte& Computer::_fetchByte(Word address)
{
	_cycles++;
	return _mem[address];
}

Word Computer::_fetchWord(Word address)
{
	Byte lowByte = _fetchByte(address);
	Byte highByte = _fetchByte(address+1);

	Word data = WORD_ADRRESS_FROM_BYTES(lowByte, highByte);

	return data;
}

Word Computer::_fetchWord()
{
	Byte lowByte = _fetchByte();
	Byte highByte = _fetchByte();

	Word data = WORD_ADRRESS_FROM_BYTES(lowByte, highByte);

	return data;
}

//pointer: register = *(WordAddress + x) x is index
Byte& Computer::addressingModeAbsoluteX()
{
	Byte lowByte = _fetchByte(); //cycle #2

	Byte highByte = _fetchByte(); //cycle #3

	Byte overFlow = GET_OVERFLOW_ONE_BYTE_ADDITION(lowByte, _cpu.getX());

	lowByte += _cpu.getX();

	if (overFlow) // if page crossed
	{
		_cycles++;  //cycle #4
	}

	//If an opcode needs to perform 16-bit math to calculate an address, then the CPU needs to use two cycles: one to add the index to the LSB, and then another to carry into the MSB.
	Word finalAddress = WORD_ADRRESS_FROM_BYTES(lowByte, highByte + overFlow);


	Byte& value = _fetchByte(finalAddress); //cycle # 4 / 5
	return value;
}

//pointer: register = *(WordAddress + y) y is index
Byte& Computer::addressingModeAbsoluteY()
{
	Byte lowByte = _fetchByte(); //cycle #2

	Byte highByte = _fetchByte(); //cycle #3

	Byte overFlow = GET_OVERFLOW_ONE_BYTE_ADDITION(lowByte, _cpu.getY());

	lowByte += _cpu.getY();

	if (overFlow) // if page crossed
	{
		_cycles++;  //cycle #4
	}

	//If an opcode needs to perform 16-bit math to calculate an address, then the CPU needs to use two cycles: one to add the index to the LSB, and then another to carry into the MSB.
	Word finalAddress = WORD_ADRRESS_FROM_BYTES(lowByte, highByte + overFlow);


	Byte& value = _fetchByte(finalAddress); //cycle # 4 / 5
	return value;
}

void Computer::updateFlagsInstructionLoad(Byte& Register, Byte newValue)
{
	Register = newValue;

	_cpu.updateFlagZero(Register);
	_cpu.updateFlagNegtive(Register);
}

//Immediate: register = Byte
Byte& Computer::addressingModeLoadImmediate()
{
	Byte& data = _fetchByte(); // cycle 2

	return data;
}

//pointer: register = *(Byte zeroPageAddress)
Byte& Computer::addressingModeLoadZeroPage()
{
	Byte adderInZP = _fetchByte(); //cycle #2
	Byte& data = _fetchByte(adderInZP);  //cycle #3
	return data;
}

//pointer and offset: register = *(ByteAddress + y)
Byte& Computer::addressingModeLoadZeroPageY()
{
	Byte zeroPageAddress = _fetchByte(); //cycle #2

	Byte finalAddress = _cpu.getY() + zeroPageAddress; //cycle #3
	_cycles++;

	Byte& data = _fetchByte(finalAddress); //cycle #4

	return data;
}

//pointer and offset: register = *(ByteAddress + x)
Byte& Computer::addressingModeLoadZeroPageX()
{
	Byte zeroPageAddress = _fetchByte(); //cycle #2

	Byte finalAddress = _cpu.getX() + zeroPageAddress; //cycle #3
	_cycles++;

	Byte& data = _fetchByte(finalAddress); //cycle #4

	return data;
}

//pointer: register = *(WordAddress)
Byte& Computer::addressingModeLoadAbsolute()
{
	Word finalAddress = _fetchWord();

	Byte& data = _fetchByte(finalAddress); //cycle #4

	return data;
}

//pointer: register = **(ByteAddress + x) x is index ????
Byte& Computer::addressingModeIndexedIndirect()
{
	Byte baseAddress = _fetchByte(); //cycle #2

	//discarding the carry.
	baseAddress += _cpu.getX(); //cycle #3
	_cycles++;

	Word finalAdrress = _fetchWord(baseAddress); //cycle # 4, 5

	Byte& data = _fetchByte(finalAdrress); // cycle #6

	return data;
}

//????
Byte& Computer::addressingModeIndirectIndexed()
{
	Byte pageZeroAddress = _fetchByte(); //cycle #2

	Byte lowByte = _fetchByte(pageZeroAddress); //cycle #3

	Byte overFlow = GET_OVERFLOW_ONE_BYTE_ADDITION(lowByte, _cpu.getY());

	lowByte += _cpu.getY();

	Byte highByte = _fetchByte(pageZeroAddress + 1);  //cycle #4

	if (overFlow) // if page crossed
	{
		_cycles++;  //cycle #5

	}

	Word finalAdrress = WORD_ADRRESS_FROM_BYTES(lowByte, highByte + overFlow);

	Byte& data = _fetchByte(finalAdrress); // cycle #5/6

	return data;
}
