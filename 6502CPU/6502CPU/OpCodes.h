#pragma once

namespace OpCodes
{
    // LDA (LoaD Accumulator)
    // Affects Flags: N Z
    constexpr Byte LDA_IMMEDIATE =      0xA9; // 2 Bytes 2 Cycles
    constexpr Byte LDA_ZERO_PAGE =      0xA5; // 2 Bytes 3 Cycles
    constexpr Byte LDA_ZERO_PAGE_X =    0xB5; // 2 Bytes 4 Cycles
    constexpr Byte LDA_ABSOLUTE =       0xAD; // 3 Bytes 4 Cycles
    constexpr Byte LDA_ABSOLUTE_X =     0xBD; // 3 Bytes 4 Cycles(+1 if page crossed)
    constexpr Byte LDA_ABSOLUTE_Y =     0xB9; // 3 Bytes 4 Cycles(+1 if page crossed)
    constexpr Byte LDA_INDEXED_INDIRECT=0xA1; // 2 Bytes 6 Cycles
    constexpr Byte LDA_INDIRECT_INDEXED=0xB1; // 2 Bytes 5 Cycles(+1 if page crossed)

    // LDX (LoaD X register)
    // Affects Flags: N Z

    constexpr Byte LDX_IMMEDIATE =      0xA2; // 2 Bytes 2 Cycles
    constexpr Byte LDX_ZERO_PAGE =      0xA6; // 2 Bytes 3 Cycles
    constexpr Byte LDX_ZERO_PAGE_Y =    0xB6; // 2 Bytes 4 Cycles
    constexpr Byte LDX_ABSOLUTE =       0xAE; // 3 Bytes 4 Cycles
    constexpr Byte LDX_ABSOLUTE_Y =     0xBE; // 3 Bytes 4 Cycles(+1 if page crossed)

    // LDY (LoaD Y register)
    // Affects Flags: N Z

    constexpr Byte LDY_IMMEDIATE =      0xA0; // 2 Bytes 2 Cycles
    constexpr Byte LDY_ZERO_PAGE =      0xA4; // 2 Bytes 3 Cycles
    constexpr Byte LDY_ZERO_PAGE_X =    0xB4; // 2 Bytes 4 Cycles
    constexpr Byte LDY_ABSOLUTE =       0xAC; // 3 Bytes 4 Cycles
    constexpr Byte LDY_ABSOLUTE_X =     0xBC; // 3 Bytes 4 Cycles(+1 if page crossed)

    // LSR(Logical Shift Right)
    // Affects Flags : N Z C
    //LSR shifts all bits right one position. 0 is shifted into bit 7 and the original bit 0 is shifted into the Carry.

    constexpr Byte  LSR_ACCUMULATOR =   0x4A;// 1 Bytes 2 Cycles
    constexpr Byte  LSR_ZERO_PAGE =     0x46;// 2 Bytes 5 Cycles
    constexpr Byte  LSR_ZERO_PAGE_X =   0x56;// 2 Bytes 6 Cycles
    constexpr Byte  LSR_ABSOLUTE =      0x4E;// 3 Bytes 6 Cycles
    constexpr Byte  LSR_ABSOLUTE_X =    0x5E;// 3 Bytes 7 Cycles

}
