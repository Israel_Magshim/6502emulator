#pragma once

#include "Memory.h"
#include "Cpu.h"
#include "OpCodes.h"
#include "iostream"

class Computer
{
public:
	void excute();
	int getCycles() const;
	Computer() = default;
	Computer(const Cpu& cpu, const Memory& mem);
	Flags getFlags() const;
	Cpu getCpu() const;
	Memory getMemory() const;

private:
	Memory _mem;
	Cpu _cpu;
	Byte& _fetchByte();
	Byte& _fetchByte(Word address);
	Word _fetchWord();
	Word _fetchWord(Word address);
	unsigned int _cycles;
	void updateFlagsInstructionLoad(Byte& Register, Byte newValue);

	// Addressing Mode:

	Byte& addressingModeAbsoluteX();
	Byte& addressingModeAbsoluteY();
	Byte& addressingModeLoadImmediate();
	Byte& addressingModeLoadZeroPage();
	Byte& addressingModeLoadZeroPageX();
	Byte& addressingModeLoadZeroPageY();
	Byte& addressingModeLoadAbsolute();
	Byte& addressingModeIndexedIndirect();
	Byte& addressingModeIndirectIndexed();
};

