#include "Cpu.h"
#include <iostream>
#include "memory.h"
#include "OpCodes.h"

bool test_LDA_ABSOLUTE_Y_WITH_OVERFLOW();
//bool test_LDA_ABSOLUTE_Y_NO_OVERFLOW();

Cpu cpu;
Memory mem;

int main()
{
	//std::cout << test_LDA_ABSOLUTE_Y_NO_OVERFLOW() << std::endl;

	//excute();

	std::cout << cpu;

	return 0;
}
void test_LDA_INDIRECT_INDEXED()
{
	// code

	Byte zPageAdress = 0xB4;
	Byte xValue = 6;

	mem[0xFFFC] = OpCodes::LDA_INDIRECT_INDEXED;
	mem[0xFFFD] = zPageAdress; //first byte
	cpu.setX(xValue);

	Byte low = mem[zPageAdress] = 0xEE;
	Byte high = mem[zPageAdress + 1] = 0x12;

	Word finalAdress = 0x12EE + cpu.getY();
	//std::cout << std::hex << std::uppercase << WORD_ADRRESS_FROM_BYTES(low, high);

	mem[finalAdress] = 8;


}

void test_LDA_INDEXED_INDIRECT()
{
	Byte baseAdress = 0xB4;
	Byte xValue = 6;

	mem[0xFFFC] = OpCodes::LDA_INDEXED_INDIRECT;
	mem[0xFFFD] = baseAdress; //first byte
	cpu.setX(xValue);

	Byte zPageAdress = xValue + baseAdress;

	Byte low = mem[zPageAdress] = 0x12;
	Byte high = mem[zPageAdress + 1] = 0xEE;

	//std::cout << std::hex << std::uppercase << WORD_ADRRESS_FROM_BYTES(low, high);

	mem[0xEE12] = 8;



}

void test_LDA_INDEXED_INDIRECTT()
{
	// code

	Byte oprand = 0xff;
	Byte xValue = 0x89;

	mem[0xFFFC] = OpCodes::LDA_INDEXED_INDIRECT;
	mem[0xFFFD] = oprand; //first byte
	cpu.setX(xValue);

	Word address = oprand + xValue;
	mem[address] = 5;
	mem[address + 1] = 5;
}

//V
bool test_LDA_ABSOLUTE_Y_WITH_OVERFLOW()
{
	mem[0xFFFC] = OpCodes::LDA_ABSOLUTE_Y;
	mem[0xFFFD] = 0xff; //first byte
	mem[0xFFFE] = 0xf0; //second byte
	// final address: f0ff
	cpu.setY(0x1);
	mem[0xf100] = 5;

	//excute();
	return 5 == cpu.getA();
}

