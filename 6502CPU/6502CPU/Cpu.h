#pragma once

#include "Definitions.h"
#include <ostream>
#include <format>


#define SP_START_ADDR   0x0100
#define SP_LAST_ADDR    0x01FF

//bit-field
class Flags {
public:
    Byte N : 1; // NEGATIVE. Set if bit 7 of the accumulator is set.
    Byte V : 1; // OVERFLOW. Set if the addition of two like-signed numbers or the subtraction of two unlike-signed numbers produces a result greater than +127 or less than -128.
    Byte B : 1; // BRK COMMAND. Set if an interrupt caused by a BRK, reset if caused by an external interrupt.
    Byte D : 1; // DECIMAL MODE. Set if decimal mode active.
    Byte I : 1; // IRQ DISABLE.  Set if maskable interrupts are disabled.
    Byte Z : 1; // ZERO.  Set if the result of the last operation (load/inc/dec/add/sub) was zero.
    Byte C : 1; // CARRY. Set if the add produced a carry, or if the subtraction produced a borrow.Also holds bits after a logical shift.


    friend std::ostream& operator<<(std::ostream& os, const Flags& dt);
    Flags();


};

class Cpu
{
public:
    Cpu();
    friend std::ostream& operator<<(std::ostream& os, const Cpu& dt);
    // IP
    Word getIP() const;
    void incIP();

    //registers
    Byte getA() const;
    void setA(Byte data);

    Byte getX() const;
    void setX(Byte data);

    Byte getY() const;
    void setY(Byte data);


    //flags
    Flags getFlags() const;
    void updateFlagZero(const Byte& Register);
    void updateFlagNegtive(const Byte& Register);
    void updateFlags();

	Byte A;		// accumulator (A)  -  Handles all arithmetic and logic.  The real heart of the system.
	Byte X, Y;	// General purpose registers with limited abilities.
	Byte SP;	// Stack pointer. $0100 to $01FF (255 addresses)
    Flags P;    // P  -  Processor status.  Holds the result of tests and flags.
    Word IP;    // program counter.
private:

    bool fNegtive(Byte Register) const;
    bool fZero(Byte Register) const;

};



