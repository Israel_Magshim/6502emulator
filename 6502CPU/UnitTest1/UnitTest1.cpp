#include "pch.h"
#include "CppUnitTest.h"
#include "../6502CPU/Definitions.h"
#include "../6502CPU/OpCodes.h"
#include "../6502CPU/Cpu.cpp"
#include "../6502CPU/Memory.cpp"
#include "../6502CPU/Computer.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(LDA)
	{
	public:

		TEST_METHOD(LDA_IMMEDIATE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDA_IMMEDIATE;
			mem[0xFFFD] = 5; //first byte

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual(pc.getCpu().A, (Byte)5);
			Assert::AreEqual(pc.getCycles(), 2);

		}

		TEST_METHOD(LDA_ZERO_PAGE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDA_ZERO_PAGE;
			mem[0xFFFD] = 5; //first byte

			mem[0x5] = 2; //first byte

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual(pc.getCpu().A, (Byte)2);
			Assert::AreEqual(pc.getCycles(), 3);
		}

		TEST_METHOD(LDA_ZERO_PAGE_X)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDA_ZERO_PAGE_X;
			mem[0xFFFD] = 5; //first byte

			cpu.setX(5);

			mem[0xA] = 2;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual(pc.getCpu().A, (Byte)2);
			Assert::AreEqual(4, pc.getCycles());
		}

		TEST_METHOD(LDA_ABSOLUTE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDA_ABSOLUTE;
			mem[0xFFFD] = 0x0f; //first byte
			mem[0xFFFE] = 0xf0; //second byte

			mem[0xF00F] = 2;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)2, pc.getCpu().A);
			Assert::AreEqual(4, pc.getCycles());
		}

		TEST_METHOD(LDA_ABSOLUTE_Y_NO_OVERFLOW)
		{
			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDA_ABSOLUTE_Y;
			mem[0xFFFD] = 0xfe; //first byte
			mem[0xFFFE] = 0xf0; //second byte
			cpu.setY(0x1);
			// final address: f0fe + 1
			mem[0xf0ff] = 5;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual(pc.getCpu().getA(), (Byte)5);
			Assert::AreEqual(4, pc.getCycles());

		}

		TEST_METHOD(LDA_ABSOLUTE_Y_WITH_OVERFLOW)
		{
			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDA_ABSOLUTE_Y;
			mem[0xFFFD] = 0xff; //first byte
			mem[0xFFFE] = 0xf0; //second byte
			cpu.setY(0x1);

			//0xff
			//0x01
			//0x100

			// final address: f0ff + 1
			mem[0xf100] = 81;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)81, pc.getCpu().getA());
			Assert::AreEqual(5, pc.getCycles());

		}

		TEST_METHOD(LDA_INDEXED_INDIRECT)
		{
			Cpu cpu;
			Memory mem;

			Byte oprand = 0xff;
			Byte xValue = 0x89;

			mem[0xFFFC] = OpCodes::LDA_INDEXED_INDIRECT;
			mem[0xFFFD] = oprand; //first byte
			cpu.setX(xValue);

			Byte zeroPageAddress = oprand + xValue; // no carry
			mem[zeroPageAddress] = 0x1;
			mem[zeroPageAddress + 1] = 0x5;

			mem[WORD_ADRRESS_FROM_BYTES(0x1, 0x5)] = 100;



			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)100, pc.getCpu().getA());
			Assert::AreEqual(6, pc.getCycles());

		}

		TEST_METHOD(LDA_INDIRECT_INDEXED_NO_OVER_FLOW)
		{
			Cpu cpu;
			Memory mem;

			// code

			Byte zPageAdress = 0x80;
			Byte yValue = 3;

			mem[0xFFFC] = OpCodes::LDA_INDIRECT_INDEXED;
			mem[0xFFFD] = zPageAdress; //first byte
			cpu.setY(yValue);

			Byte low = mem[zPageAdress] = 0x20;
			Byte high = mem[zPageAdress + 1] = 0x40;

			Word finalAdress = 0x4020 + yValue;
			//std::cout << std::hex << std::uppercase << WORD_ADRRESS_FROM_BYTES(low, high);

			mem[finalAdress] = 8;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual(5, pc.getCycles());
			Assert::AreEqual((Byte)8, pc.getCpu().getA());

		}

		TEST_METHOD(LDA_INDIRECT_INDEXED_WITH_OVER_FLOW)
		{
			Cpu cpu;
			Memory mem;

			// code

			Byte zPageAdress = 0x02;
			Byte yValue = 0xff;

			mem[0xFFFC] = OpCodes::LDA_INDIRECT_INDEXED;
			mem[0xFFFD] = zPageAdress; //first byte
			cpu.setY(yValue);

			Byte low = mem[zPageAdress] = 0x02;
			Byte high = mem[zPageAdress + 1] = 0x80;

			//  11
			//0xff
			//0x02 =
			//0x101
			//


			Word finalAdress = 0x8101;
			//std::cout << std::hex << std::uppercase << WORD_ADRRESS_FROM_BYTES(low, high);

			mem[finalAdress] = 8;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual(6, pc.getCycles());
			Assert::AreEqual((Byte)8, pc.getCpu().getA());

		}

	};

	TEST_CLASS(LDX)
	{
	public:
		TEST_METHOD(LDX_IMMEDIATE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDX_IMMEDIATE;
			mem[0xFFFD] = 5; //first byte

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)5, pc.getCpu().X);
			Assert::AreEqual(2, pc.getCycles());

		}

		TEST_METHOD(LDX_ZERO_PAGE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDX_ZERO_PAGE;
			mem[0xFFFD] = 5; //first byte

			mem[0x5] = 2; //first byte

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)2, pc.getCpu().X);
			Assert::AreEqual(3, pc.getCycles());
		}

		TEST_METHOD(LDX_ZERO_PAGE_Y)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDX_ZERO_PAGE_Y;
			mem[0xFFFD] = 5; //first byte

			cpu.setY(5);

			mem[0xA] = 2;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)2, pc.getCpu().X);
			Assert::AreEqual(4, pc.getCycles());
		}

		TEST_METHOD(LDX_ABSOLUTE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDX_ABSOLUTE;
			mem[0xFFFD] = 0x0f; //first byte
			mem[0xFFFE] = 0xf0; //second byte

			mem[0xF00F] = 2;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)2, pc.getCpu().X);
			Assert::AreEqual(4, pc.getCycles());
		}

		TEST_METHOD(LDX_ABSOLUTE_Y_NO_OVERFLOW)
		{
			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDX_ABSOLUTE_Y;
			mem[0xFFFD] = 0xfe; //first byte
			mem[0xFFFE] = 0xf0; //second byte
			cpu.setY(0x1);
			// final address: f0fe + 1
			mem[0xf0ff] = 5;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)5, pc.getCpu().getX());
			Assert::AreEqual(4, pc.getCycles());

		}

		TEST_METHOD(LDX_ABSOLUTE_Y_WITH_OVERFLOW)
		{
			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDX_ABSOLUTE_Y;
			mem[0xFFFD] = 0xff; //first byte
			mem[0xFFFE] = 0xf0; //second byte
			cpu.setY(0x1);

			//0xff
			//0x01
			//0x100

			// final address: f0ff + 1
			mem[0xf100] = 81;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)81, pc.getCpu().getX());
			Assert::AreEqual(5, pc.getCycles());

		}

	};

	TEST_CLASS(LDY)
	{
	public:
		TEST_METHOD(LDY_IMMEDIATE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDY_IMMEDIATE;
			mem[0xFFFD] = 5; //first byte

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)5, pc.getCpu().Y);
			Assert::AreEqual(2, pc.getCycles());

		}

		TEST_METHOD(LDY_ZERO_PAGE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDY_ZERO_PAGE;
			mem[0xFFFD] = 5; //first byte

			mem[0x5] = 2; //first byte

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)2, pc.getCpu().Y);
			Assert::AreEqual(3, pc.getCycles());
		}

		TEST_METHOD(LDY_ZERO_PAGE_X)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDY_ZERO_PAGE_X;
			mem[0xFFFD] = 5; //first byte

			cpu.setX(5);

			mem[0xA] = 2;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)2, pc.getCpu().Y);
			Assert::AreEqual(4, pc.getCycles());
		}

		TEST_METHOD(LDY_ABSOLUTE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDY_ABSOLUTE;
			mem[0xFFFD] = 0x0f; //first byte
			mem[0xFFFE] = 0xf0; //second byte

			mem[0xF00F] = 2;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)2, pc.getCpu().Y);
			Assert::AreEqual(4, pc.getCycles());
		}

		TEST_METHOD(LDY_ABSOLUTE_X_NO_OVERFLOW)
		{
			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDY_ABSOLUTE_X;
			mem[0xFFFD] = 0xfe; //first byte
			mem[0xFFFE] = 0xf0; //second byte
			cpu.setX(0x1);
			// final address: f0fe + 1
			mem[0xf0ff] = 5;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)5, pc.getCpu().getY());
			Assert::AreEqual(4, pc.getCycles());

		}

		TEST_METHOD(LDY_ABSOLUTE_X_WITH_OVERFLOW)
		{
			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LDY_ABSOLUTE_X;
			mem[0xFFFD] = 0xff; //first byte
			mem[0xFFFE] = 0xf0; //second byte
			cpu.setX(0x1);

			//0xff
			//0x01
			//0x100

			// final address: f0ff + 1
			mem[0xf100] = 81;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)81, pc.getCpu().getY());
			Assert::AreEqual(5, pc.getCycles());

		}

	};

	TEST_CLASS(LSR)
	{
	public:
		TEST_METHOD(LSR_ACCUMULATOR_CARRY_FLAG_IS_SET)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LSR_ACCUMULATOR;

			Byte aValue = 0b1001;
			cpu.setA(aValue);
			
			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)0b0100, pc.getCpu().A);
			Assert::AreEqual(2, pc.getCycles());
			Assert::AreEqual((Byte)1, pc.getCpu().getFlags().C);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().Z);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().N);

		}

		TEST_METHOD(LSR_ACCUMULATOR_CARRY_FLAG_NOT_SET)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LSR_ACCUMULATOR;

			Byte aValue = 0b1010;
			cpu.setA(aValue);

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)0b0101, pc.getCpu().A);
			Assert::AreEqual(2, pc.getCycles());
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().C);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().Z);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().N);

		}

		TEST_METHOD(LSR_ZERO_PAGE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LSR_ZERO_PAGE;
			mem[0xFFFD] = 0x5;

			Byte value = 0b1010;
			mem[0x5] = value;

			Computer pc(cpu, mem);
			pc.excute();
			// pc.getMemory() check i cant change memory
			pc.getMemory()[0x5] = 0;
			Assert::AreEqual((Byte)0b0101, pc.getMemory()[0x5]);
			Assert::AreEqual(5, pc.getCycles());
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().C);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().Z);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().N);

		}

		TEST_METHOD(LSR_ZERO_PAGE_X)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LSR_ZERO_PAGE_X;
			mem[0xFFFD] = 0x5;
			cpu.X = 0x5;
			Byte value = 0b1010;
			mem[0xA] = value;

			Computer pc(cpu, mem);
			pc.excute();
			// pc.getMemory() check i cant change memory
			pc.getMemory()[0xA] = 0;
			Assert::AreEqual((Byte)0b0101, pc.getMemory()[0xA]);
			Assert::AreEqual(6, pc.getCycles());
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().C);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().Z);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().N);

		}

		TEST_METHOD(LSR_ABSOLUTE)
		{

			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LSR_ABSOLUTE;
			mem[0xFFFD] = 0x0f; //first byte
			mem[0xFFFE] = 0xf0; //second byte

			mem[0xF00F] = 2;

			Computer pc(cpu, mem);
			pc.excute();
			Assert::AreEqual((Byte)1, pc.getMemory()[0xF00F]);
			Assert::AreEqual(6, pc.getCycles());
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().C);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().Z);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().N);
		}

		TEST_METHOD(LSR_ABSOLUTE_X)
		{
			Cpu cpu;
			Memory mem;
			mem[0xFFFC] = OpCodes::LSR_ABSOLUTE_X;
			mem[0xFFFD] = 0xff; //first byte
			mem[0xFFFE] = 0xf0; //second byte
			cpu.setX(0x1);

			mem[0xf100] = 2;

			Computer pc(cpu, mem);
			pc.excute();

			Assert::AreEqual((Byte)1, pc.getMemory()[0xf100]);
			Assert::AreEqual(7, pc.getCycles());
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().C);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().Z);
			Assert::AreEqual((Byte)0, pc.getCpu().getFlags().N);

		}

	};
}
